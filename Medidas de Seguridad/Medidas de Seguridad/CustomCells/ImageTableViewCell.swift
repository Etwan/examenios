//
//  ImageTableViewCell.swift
//  Medidas de Seguridad
//
//  Created by Oscar Sevilla on 20/08/21.
//

import UIKit

protocol ImageTableViewCellDelegate: AnyObject {
    func selectImage()
}

class ImageTableViewCell: UITableViewCell {

    weak var delegate: ImageTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func takeSelfie(_ sender: Any) {
        print("Tomando selfie")
        delegate?.selectImage()
    }
    
}
