//
//  TextTableViewCell.swift
//  Medidas de Seguridad
//
//  Created by Oscar Sevilla on 20/08/21.
//

import UIKit

protocol TextTableViewCellDelegate: AnyObject {
    func validate(invalid: Bool, name: String)
}

class TextTableViewCell: UITableViewCell {

    @IBOutlet var nameTextfield: UITextField!
    
    weak var delegate: TextTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTextfield.addTarget(self, action: #selector(validator), for: .editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc fileprivate func validator(){
        let predicateTest = NSPredicate(format: "SELF MATCHES %@", "^(([^ ]?)(^[a-zA-Z].*[a-zA-Z]$)([^ ]?))$")
        if nameTextfield.text!.count > 1{
            if predicateTest.evaluate(with: nameTextfield.text){
                nameTextfield.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                delegate?.validate(invalid: false, name: nameTextfield.text!)
            }else{
                nameTextfield.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                delegate?.validate(invalid: true, name: nameTextfield.text!)
            }
        }
    }
    
}
