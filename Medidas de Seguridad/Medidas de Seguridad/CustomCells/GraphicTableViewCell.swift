//
//  GraphicTableViewCell.swift
//  Medidas de Seguridad
//
//  Created by Oscar Sevilla on 20/08/21.
//

import UIKit

protocol GraphicTableViewCellDelegate: AnyObject {
    func showGraphics()
}

class GraphicTableViewCell: UITableViewCell {

    weak var delegate: GraphicTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    @IBAction func presentGraphicsAction(_ sender: Any){
        delegate?.showGraphics()
    }
}
