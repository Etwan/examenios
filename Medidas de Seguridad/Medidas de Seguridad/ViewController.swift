//
//  ViewController.swift
//  Medidas de Seguridad
//
//  Created by Oscar Sevilla on 20/08/21.
//

import UIKit
import AVFoundation
import FirebaseStorage

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate,
                      UINavigationControllerDelegate {
    
//  Para la tabla
    @IBOutlet var table: UITableView!
        
// Para seleccionar imagenes
    private var urlImage: String?
    private var name: String?
    private var imgData: Data?
    private var img: UIImage?
    private var isCamera = true

//  para guardar las imagenes en Firebase
    private let storage = Storage.storage().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Dashboard"
        table.register(UINib.init(nibName: "TextTableViewCell", bundle: .main), forCellReuseIdentifier: "TextTableViewCell")
        table.register(UINib.init(nibName: "ImageTableViewCell", bundle: .main), forCellReuseIdentifier: "ImageTableViewCell")
        table.register(UINib.init(nibName: "GraphicTableViewCell", bundle: .main), forCellReuseIdentifier: "GraphicTableViewCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: TextTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TextTableViewCell", for: indexPath) as! TextTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        }else{
            if indexPath.row == 1{
                let cell: ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                return cell
            }else{
                let cell: GraphicTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GraphicTableViewCell", for: indexPath) as! GraphicTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            navigationController?.pushViewController(GraphicsViewController(), animated: true)
        }
    }
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        view.endEditing(true)
        picker.dismiss(animated: true, completion: nil)
        if isCamera{
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            self.img = image
        }else {
            guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
            self.img = image
        }
        
        guard let imageData = self.img!.jpegData(compressionQuality: 1.0) else{ return }
        imgData = imageData
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
        
    func storagePhoto(image: UIImage, uploadData: Data){
        storage.child("\(self.name ?? "foto").jpg").putData(uploadData, metadata: nil, completion: {_, error in
            guard error == nil else {
                print("Failed to upload")
                return
            }
        })
    }
    
    @IBAction func uploadImage(_ sender: Any){
        print("Subiendo imagen")
        DispatchQueue.main.async {
            self.storagePhoto(image: self.img!, uploadData: self.imgData!)
        }
    }
    
}

extension ViewController: TextTableViewCellDelegate{
    func validate(invalid: Bool, name: String) {
        if invalid {
            let alert = UIAlertController(title: "Nombre inválido", message: "Sólo se aceptan caracteres álfabeticos", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.name = name
        }
    }
}

extension ViewController: GraphicTableViewCellDelegate{
    func showGraphics() {
        navigationController?.pushViewController(GraphicsViewController(), animated: true)
    }
}

extension ViewController: ImageTableViewCellDelegate{
    func selectImage(){
        let message = NSLocalizedString("Select an option", comment: "")
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { [unowned self] _ in
            isCamera = true
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = self
            present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default) { [unowned self] _ in
            isCamera = false
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            present(picker, animated: true, completion: nil)
        })
        present(alert, animated: true, completion: nil)
    }
}
