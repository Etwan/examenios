//
//  GraphicsViewController.swift
//  Medidas de Seguridad
//
//  Created by Oscar Sevilla on 20/08/21.
//

import UIKit
import SwiftyJSON
import Alamofire
import Charts

class GraphicsViewController: UIViewController {

    @IBOutlet var questionOne: UILabel!
    @IBOutlet var questionTwo: UILabel!
    @IBOutlet var answerOne: UILabel!
    @IBOutlet var answerTwo: UILabel!
    
    @IBOutlet var enterpriseOne: UILabel!
    @IBOutlet var enterpriseTwo: UILabel!
    @IBOutlet var enterpriseThree: UILabel!
    @IBOutlet var enterpriseFour: UILabel!
    @IBOutlet var enterpriseFive: UILabel!
    @IBOutlet var enterpriseSix: UILabel!
    
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var pieChartView2: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Gráficas"
        loadData()
    }
    
    func loadData(){
        AF.request("https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld").responseJSON { response in
            switch response.result{
            case .success(let value):
                let json = JSON(value)
                debugPrint(json)
                
                var percetnageData = [Double]()
                var percetnageData2 = [Double]()
                
                if let text = json["questions"][0]["text"].string {
                    self.questionOne.text = text
                }
                if let text = json["questions"][1]["text"].string {
                    self.questionTwo.text = text
                }
                
                if let answer = json["questions"][0]["chartData"][1]["text"].string{
                    if let percetnage = json["questions"][0]["chartData"][1]["percetnage"].int{
                        percetnageData.append(Double(percetnage))
                    self.answerTwo.text = "\(answer) \(percetnage)%"
                    }
                }
                
                if let answer = json["questions"][0]["chartData"][0]["text"].string{
                    if let percetnage = json["questions"][0]["chartData"][0]["percetnage"].int{
                        percetnageData.append(Double(percetnage))
                    self.answerOne.text = "\(answer) \(percetnage)%"
                    }
                }
                
                self.setChart1(percetnage: percetnageData)
                
                
                if let text = json["questions"][1]["chartData"][0]["text"].string {
                    if let percetnage = json["questions"][1]["chartData"][0]["percetnage"].int{
                        percetnageData2.append(Double(percetnage))
                        self.enterpriseOne.text = "\(text) \(percetnage)%"
                    }
                }
                if let text = json["questions"][1]["chartData"][1]["text"].string {
                    if let percetnage = json["questions"][1]["chartData"][1]["percetnage"].int{
                        percetnageData2.append(Double(percetnage))
                        self.enterpriseFour.text = "\(text) \(percetnage)%"
                    }
                }
                if let text = json["questions"][1]["chartData"][2]["text"].string {
                    if let percetnage = json["questions"][1]["chartData"][2]["percetnage"].int{
                        percetnageData2.append(Double(percetnage))
                        self.enterpriseTwo.text = "\(text) \(percetnage)%"
                    }
                }
                if let text = json["questions"][1]["chartData"][3]["text"].string {
                    if let percetnage = json["questions"][1]["chartData"][3]["percetnage"].int{
                        percetnageData2.append(Double(percetnage))
                        self.enterpriseFive.text = "\(text) \(percetnage)%"
                    }
                }
                if let text = json["questions"][1]["chartData"][4]["text"].string {
                    if let percetnage = json["questions"][1]["chartData"][4]["percetnage"].int{
                        percetnageData2.append(Double(percetnage))
                        self.enterpriseThree.text = "\(text) \(percetnage)%"
                    }
                }
                if let text = json["questions"][1]["chartData"][5]["text"].string {
                    if let percetnage = json["questions"][1]["chartData"][5]["percetnage"].int{
                        percetnageData2.append(Double(percetnage))
                        self.enterpriseSix.text = "\(text) \(percetnage)%"
                    }
                }
                
                
                
                self.setChart2(percetnage: percetnageData2)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setChart1(percetnage: [Double]) {
        var entries = [PieChartDataEntry]()
        for (index, value) in percetnage.enumerated() {
            let entry = PieChartDataEntry()
            entry.y = value
            entries.append( entry)
        }

        // 3. chart setup
        let set = PieChartDataSet(entries: entries, label: "")
        var colors: [UIColor] = []
        colors.append(#colorLiteral(red: 1, green: 0.1764705882, blue: 0.3333333333, alpha: 1))
        colors.append(#colorLiteral(red: 0.4659959078, green: 0.7426485419, blue: 0.7305559516, alpha: 1))
        set.colors = colors
        
        let data = PieChartData(dataSet: set)
        pieChartView.data = data
        pieChartView.entryLabelColor = UIColor.clear
        pieChartView.noDataText = "No data available"
        pieChartView.holeRadiusPercent = 0.8
        pieChartView.holeColor = UIColor.clear
        pieChartView.transparentCircleColor = UIColor.clear
    }
    
    func setChart2(percetnage: [Double]) {
        var entries = [PieChartDataEntry]()
        for (index, value) in percetnage.enumerated() {
            let entry = PieChartDataEntry()
            entry.y = value
            entries.append( entry)
        }

        // 3. chart setup
        let set = PieChartDataSet(entries: entries, label: "")
        var colors: [UIColor] = []
        colors.append(#colorLiteral(red: 0.4659959078, green: 0.7426485419, blue: 0.7305559516, alpha: 1))
        colors.append(#colorLiteral(red: 1, green: 0.1764705882, blue: 0.3333333333, alpha: 1))
        colors.append(#colorLiteral(red: 1, green: 0.5602541492, blue: 0.3333333333, alpha: 1))
        colors.append(#colorLiteral(red: 0, green: 0.5414664149, blue: 0.5966866016, alpha: 1))
        colors.append(UIColor.yellow)
        colors.append(#colorLiteral(red: 0.4124245048, green: 0.8191624284, blue: 1, alpha: 1))
        set.colors = colors
        
        let data = PieChartData(dataSet: set)
        pieChartView2.data = data
        pieChartView2.entryLabelColor = UIColor.clear
        pieChartView2.noDataText = "No data available"
        pieChartView2.holeRadiusPercent = 0.8
        pieChartView2.holeColor = UIColor.clear
        pieChartView2.transparentCircleColor = UIColor.clear
      }
    
}

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        return nil
    }
}
